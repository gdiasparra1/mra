package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	private int worldX;
	private int worldY;
	private List <String> obstacles = new ArrayList<>();
	private int[][] obstaclesInt;
	private int numberOfObstacles=0;
	private int posX;
	private int posY;
	private String facing;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		posX=0;
		posY=0;
		facing="N";
		
		worldX=planetX;
		worldY=planetY;
		numberOfObstacles=planetObstacles.size();
		obstaclesInt= new int[2][numberOfObstacles];
		
		convertToInt(planetObstacles);	//converto in interi le coordinate degli ostacoli
	}

	private void convertToInt(List<String> planetObstacles) {
		String xCoord;
		String yCoord;
		
		for (int i=0;i< planetObstacles.size();i++) {
			xCoord=planetObstacles.get(i).substring(1, 2);		
			yCoord=planetObstacles.get(i).substring(3,4);
			
			obstaclesInt[0][i]=Integer.valueOf(xCoord);
			obstaclesInt[1][i]=Integer.valueOf(yCoord);
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean isFound=false;	//se viene trovato che nelle posizioni specificate c'� un ostacolo � true
		
		for (int i=0;i<numberOfObstacles;i++) {
			if(obstaclesInt[0][i]==x && obstaclesInt[1][i]==y) {
				isFound=true;			
				break;
			}
		}		
		return isFound;			
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String landingStatus="("+Integer.toString(posX)+","+Integer.toString(posY);
		
		for (int i=0;i<commandString.length();i++) {
			
			String singleCommand=commandString.substring(i, i+1);
					
				if(singleCommand.isEmpty()) {
					landingStatus=landingStatus+","+facing+")";
					
				}else if(singleCommand.equals("r") && facing.equals("N")) {
					facing="E";
					landingStatus=landingStatus+",E)";
				}else if(singleCommand.equals("l") && facing.equals("N")) {
					facing="W";
					landingStatus=landingStatus+",W)";
				}else if(singleCommand.equals("l") && facing.equals("S")) {
					facing="E";
					landingStatus=landingStatus+",E)";
				}else if(singleCommand.equals("r") && facing.equals("S")) {
					facing="W";
					landingStatus=landingStatus+",W)";
				}else if(singleCommand.equals("r") && facing.equals("W")) {
					facing="N";
					landingStatus=landingStatus+",N)";
				}else if(singleCommand.equals("l") && facing.equals("W")) {
					facing="S";
					landingStatus=landingStatus+",S)";
				}else if(singleCommand.equals("r") && facing.equals("E")) {
					facing="S";
					landingStatus=landingStatus+",S)";
				}else if(singleCommand.equals("l") && facing.equals("E")) {
					facing="N";
					landingStatus=landingStatus+",N)";
				}
				
				
				if(singleCommand.equals("f")) {
					if(facing.equals("N")) {
						if(posY==9 || posX==9) {
							posY=0;
						}else posY++;
					}
					
					else if(facing.equals("E")) {
						if(posX==9 || posY==9) {
							posX=0;
						}else posX++;	
					}else if(facing.equals("S")) {
						posY--;	
					}else if(facing.equals("W")) {
						posX--;	
					}
				}
				
				if(singleCommand.equals("b")) {
					if(facing.equals("N")) {
						if (posY==0) {
							posY=9;
						}else posY--;
					}else if(facing.equals("E")) {
						if(posX==0) {
							posX=9;
						}else posX--;	
					}else if(facing.equals("S")) {
						posY++;	
					}else if(facing.equals("W")) {
						if (posX==9) {
							posX=0;
						}else posX++;
					}
					

				}
			
		}
		
		landingStatus="("+Integer.toString(posX)+","+Integer.toString(posY)+","+facing+")";
		
		return landingStatus;
	}

	public void setPosFacing(String face) {
		facing=face;	
	}

	public void setPosX(int x) {
		posX=x;
	}

	public void setPosY(int y) {
		posY=y;		
	}
	
	

}
