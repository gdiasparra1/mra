package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;



public class MarsRoverTest {

	@Test
	public void testFindObstacle() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals(true, rover.planetContainsObstacleAt(4,7));
		
	}
	
	@Test
	public void testDoNotFindInesistentObstacle() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals(false, rover.planetContainsObstacleAt(2,8));
		
	}
	
	@Test
	public void turningRightFromFacingNShouldReturnE() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command="r";
		
		assertEquals("(0,0,E)", rover.executeCommand(command));
		
	}
	
	@Test
	public void turningLeftFromFacingNShouldReturnW() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command="l";
		
		assertEquals("(0,0,W)", rover.executeCommand(command));
		
	}

	@Test
	public void turningLeftFromFacingSShouldReturnE() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command="l";
		rover.executeCommand(command);
		rover.executeCommand(command); 
		
		assertEquals("(0,0,E)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingForwardFacingNShouldIncrementY() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String command="f";		
		assertEquals("(0,1,N)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingBackwardShouldDecrementX() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setPosX(5);
		rover.setPosY(8);
		rover.setPosFacing("E");
		
		String command="b";		
		
		
		assertEquals("(4,8,E)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingForwardFacingEShouldIncrementX() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setPosX(5);
		rover.setPosY(8);
		rover.setPosFacing("E");
		
		String command="f";		
		assertEquals("(6,8,E)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingForwardFacingSShouldDecrementY() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setPosX(5);
		rover.setPosY(8);
		rover.setPosFacing("S");
		
		String command="f";		
		assertEquals("(5,7,S)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingForwardFacingWShouldDecrementX() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setPosX(5);
		rover.setPosY(8);
		rover.setPosFacing("W");
		
		String command="f";		
		assertEquals("(4,8,W)", rover.executeCommand(command));
		
	}
	
	@Test
	public void movingBackwardFacingWShouldIncrementX() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setPosX(5);
		rover.setPosY(8);
		rover.setPosFacing("W");
		
		String command="b";		
		assertEquals("(6,8,W)", rover.executeCommand(command));
		
	}
	
	
	@Test
	public void movingCombinedShouldBeExecutedIndividually() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);

		String command="ffrff";		
		assertEquals("(2,2,E)", rover.executeCommand(command));
		
	}
	
	
	@Test
	public void goToTheOtherSideFacingNfrom09() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);

		String command="b";		
		assertEquals("(0,9,N)", rover.executeCommand(command));
		
	}
	
	@Test
	public void goToTheOtherSideFacingWfrom90() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		rover.setPosX(9);
		rover.setPosY(0);
		rover.setPosFacing("W");
		
		String command="b";		
		assertEquals("(0,0,W)", rover.executeCommand(command));
		
	}
	
	@Test
	public void goToTheOtherSideFacingEfrom09() throws MarsRoverException {
		List <String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		rover.setPosX(0);
		rover.setPosY(9);
		rover.setPosFacing("E");
		
		String command="b";		
		assertEquals("(9,9,E)", rover.executeCommand(command));
		
	}
	
	
	
	
	
	
	
	
}
